# Simple App-Web Integration.

## What This Application Does.
This project has two interfaces. One an Android app and other a Web App. You enter a number from the app and then you enter a number from the web app. The web application collects data from both inputs and processes them. You then recieve a confirmation message of the result on Whatsapp & via SMS via a Twilio API client. 

## The Stack
### Android
- Flutter/Dart
### Web 
- HTML/JavaScript
- Node JS
- Express
### Backend
- Firebase Firestore

## Running The Application
### Android
- First Install the apk file on your android device. And input data from that.
### Web
- From the YEStack Web Folder, run terminal.
- Make sure you have node installed on your device.
- If you dont have node on Ubuntu, run
-       $sudo apt-get install nodejs
- Then run start the node server
-       node app.js
-Then on your browser, type in:
-       http://localhost:3000/
- Input data, click on Add Number And then click on submit.
- You then recieve a confirmation message of the result on Whatsapp & via SMS via a Twilio API client.