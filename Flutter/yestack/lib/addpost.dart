import 'package:flutter/material.dart';
import 'constants.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

var text1;
int text2;

class addpost extends StatefulWidget {
  @override
  _addpostState createState() => _addpostState();
}

class _addpostState extends State<addpost> {
  final _firestore = Firestore.instance;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 30,
              ),
              Text(
                'Enter details',
                style: logo.copyWith(fontSize: 42, color: Colors.black),
              ),
              TextField(
                decoration: InputDecoration(
                    hintText: 'number',
                    hintStyle:
                        logo.copyWith(fontSize: 24, color: Colors.black26)),
                onChanged: (value) {
                  text1 = value;
                },
              ),
              TextField(
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                    hintText: 'number1',
                    hintStyle:
                        logo.copyWith(fontSize: 24, color: Colors.black26)),
                onChanged: (value) {
                  text2 = int.parse(value);
                },
              ),
              SizedBox(
                height: 50,
              ),
              FlatButton(
                child: Text(
                  'post',
                  style: logo.copyWith(color: Colors.blue, fontSize: 20),
                ),
                onPressed: () {
                  _firestore.collection('data').document('alldata').updateData({
                    'number': text1,
                    'number1': text2,
                  });
                  Navigator.pushNamed(context, '/feed');
                },
              ),
              Expanded(
                child: Container(),
              ),
              SizedBox(
                height: 10,
              )
            ],
          ),
        ),
      ),
    );
  }
}
