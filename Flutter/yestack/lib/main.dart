import 'package:flutter/material.dart';
import 'addpost.dart';
import 'feed.dart';

void main() {
  runApp(MecLabs());
}

class MecLabs extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      initialRoute: '/',
      routes: {
        '/': (context) => addpost(),
        '/feed': (context) => Feed(),
      },
    );
  }
}
